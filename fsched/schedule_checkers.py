"""Tools for analyzing a festival schedule.

*schedules* are are lists of *tracks*. *tracks* are lists of *volunteers* where
each position indicates a time-slot. So, for example, a simple two-track schedule
with 4 half-hour slots each might look like this:

    saturday = [
        ['fred', 'fred', 'betty', 'betty'],
        ['wilma', 'wilma', 'barney', 'wilma'],
    ]
"""

from itertools import groupby


def double_bookings(schedule):
    """Generate a sequence of (slot, name) tuples indicating where someone is
    double-booked.
    """
    slots = (sorted(slot) for slot in zip(*schedule))
    for idx, slot in enumerate(slots):
        for (name, occur) in groupby(slot):
            if len(list(occur)) > 1:
                yield (idx, name)
