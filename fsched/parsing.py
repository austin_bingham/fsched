from itertools import chain
import re

from fsched.strings import string_range


COL_RANGE_RE = re.compile('[A-Za-z]+\-[A-Za-z]+')
ROW_RANGE_RE = re.compile('\d+-\d+')


def _parse_col_range(rng):
    if re.match(COL_RANGE_RE, rng) is None:
        raise ValueError(
            'Column range {} does not follow the pattern {}'.format(
                rng,
                COL_RANGE_RE.pattern))

    rng = rng.upper()
    start, stop = tuple(rng.split('-'))
    return string_range(start, stop)


def parse_cols(data):
    ranges = [_parse_col_range(rng)
              for rng in data.split(',')]
    return chain(*ranges)


def _parse_row_range(rng):
    if re.match(ROW_RANGE_RE, rng) is None:
        raise ValueError(
            'Row range {} does not follow the pattern {}'.format(
                rng,
                ROW_RANGE_RE.pattern))
    start, stop = tuple(int(t) for t in rng.split('-'))
    start, stop = min(start, stop), max(start, stop)
    return range(start, stop + 1)


def parse_rows(data):
    ranges = (_parse_row_range(rng)
              for rng in data.split(','))
    return chain(*ranges)
