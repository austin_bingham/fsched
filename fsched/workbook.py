"""Load a schedule from an excel file and check it for errors.
"""

import openpyxl


def cell_to_name(sheet, col, row):
    "Extract a name from a cell."
    val = sheet[col + str(row)].value
    if val is None:
        val = ''
    return val.encode('utf-8')


def sheet_to_schedule(sheet, cols, rows):
    "Convert a sheet to a schedule."
    return [[cell_to_name(sheet, col, row)
             for row in rows]
            for col in cols]


def workbook_to_schedules(path,
                          cols,
                          rows):
    "Convert a workbook to a sequence of (name, schedule) tuples."

    wb = openpyxl.load_workbook(path)

    return ((sheet.title, sheet_to_schedule(sheet, cols, rows))
            for sheet
            in wb.worksheets)
