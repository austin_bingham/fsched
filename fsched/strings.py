from itertools import chain, count, dropwhile, product, takewhile
import string


def all_strings(length=None):
    """Generate all strings, optionally limited to a certain length.

    When called with no argument, this generates the sequence of all possible
    strings. It starts with the empty string, followed by strings of length 1,
    then length 2, and so forth. For each size, the strings are lexically
    ordered.

    When called with an argument, this generates just the sequence of strings
    (in lexical order) of that size.

    Args:
        length: The length of strings to generate, or None.

    Generates: An iterable sequence of strings.
    """
    if length is None:
        for i in count():
            yield from all_strings(i)
    else:
        yield from (
            ''.join(chars)
            for chars in
            product(string.ascii_uppercase, repeat=length))


def string_range(start, stop):
    """All strings, in order, from `start` to `stop`, inclusive.

    If stop is less than start, then they will be reversed. So the resulting
    sequence will always go from the smaller to the larger of start and stop.

    """
    start, stop = (start, stop) if _less_than(start, stop) else (stop, start)

    strings = chain(*(all_strings(l)
                      for l
                      in range(len(start),
                               len(stop) + 1)))
    candidates = dropwhile(
        lambda s: s != start, strings)
    return chain(takewhile(lambda s: s != stop, candidates), (stop,))


def _less_than(a, b):
    """Determines if `a` is less than `b`.

    In this case, a string is less than another if:

       a) It is shorter or
       b) If the same length, if it's lexically earlier
    """
    if len(a) < len(b):
        return True
    elif len(b) < len(a):
        return False
    else:
        return a < b
