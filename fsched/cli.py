import sys

import docopt

import fsched.parsing
import fsched.schedule_checkers
import fsched.workbook

OPTIONS = """fsched

Usage: fsched [--help] <command> [<args> ...]

options:
  --help  Show this screen

Available commands:
  check
  help
"""


def handle_check(config):
    """usage: fsched check <worksheet> <cols> <rows>

Load a schedule from an excel worksheet and check it for problems.
    """
    filename = config['<worksheet>']
    cols = fsched.parsing.parse_cols(config['<cols>'])
    rows = fsched.parsing.parse_rows(config['<rows>'])
    schedules = fsched.workbook.workbook_to_schedules(
        filename, cols, rows)
    for (name, schedule) in schedules:
        print('Double-bookings for {}'.format(name))
        print('\n'.join(
            ('\tslot {}, name {}'.format(idx, name)
             for idx, name
             in fsched.schedule_checkers.double_bookings(schedule)
             if len(name) != 0)))


def handle_help(config):
    """usage: fsched help [<command>]

Get the top-level help, or help for <command> if specified.
"""
    command = config['<command>']
    if not command:
        options = OPTIONS
    elif command not in COMMAND_HANDLER_MAP:
        # LOG.error('"{}" is not a valid cosmic-ray command'.format(command))
        options = OPTIONS
    else:
        options = COMMAND_HANDLER_MAP[command].__doc__

    return docopt.docopt(options,
                         ['--help'],
                         version='cosmic-ray v.2')

COMMAND_HANDLER_MAP = {
    'check': handle_check,
    'help': handle_help
}


def main(argv=None):
    if argv is None:
        argv = sys.argv[1:]

    arguments = docopt.docopt(
        OPTIONS,
        argv=argv,
        version='fsched 0.0.1')

    command = arguments['<command>']
    try:
        handler = COMMAND_HANDLER_MAP[command]
    except KeyError:
        handler = handle_help
        argv['help']

    sub_config = docopt.docopt(
        handler.__doc__,
        argv,
        version='fsched v.0.0.1')

    sys.exit(handler(sub_config))

if __name__ == '__main__':
    main()
