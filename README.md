# fsched: tools for managing a festival schedule

## Quickstart

1. Install the program:

        python setup.py install

2. Check a schedule defined in an Excel worksheet:

        fsched check my_schedule.xlsx

Note that this currently presumes a pretty specific format for the workbook,
with certain rows intentionally left blank and stuff. Maybe we'll make this more
flexible in the future.

## Tests

To run the tests:

    python tests.py