from itertools import chain
import unittest

from fsched.parsing import parse_cols, parse_rows
from fsched.schedule_checkers import double_bookings


double_booked = [
    chain(['a'] * 4,
          ['b'] * 4,
          ['c'] * 4,),
    chain(['d'] * 4,
          ['e'] * 2,
          ['b'] * 2,
          ['f'] * 4,)
]

not_double_booked = [
    chain(['a'] * 4,
          ['b'] * 4,
          ['c'] * 4,),
    chain(['d'] * 4,
          ['e'] * 2,
          ['f'] * 3,
          ['g'] * 3)
]


class TestDoubleBookings(unittest.TestCase):
    def test_is_double_booked(self):
        result = list(double_bookings(double_booked))
        self.assertEqual(len(result), 2)

    def test_is_not_double_booked(self):
        result = list(double_bookings(not_double_booked))
        self.assertEqual(len(result), 0)


class TestParseCols(unittest.TestCase):
    def test_parse_single(self):
        result = parse_cols('A-D')
        self.assertListEqual(
            list(result),
            ['A', 'B', 'C', 'D'])

    def test_parse_multiple(self):
        result = parse_cols('A-C,F-I')
        self.assertListEqual(
            list(result),
            ['A', 'B', 'C', 'F', 'G', 'H', 'I'])

    def test_misordered(self):
        self.assertListEqual(
            list(parse_cols('C-A')),
            list(parse_cols('A-C')))

    def test_lowercase(self):
        self.assertListEqual(
            list(parse_cols('f-V')),
            list(parse_cols('F-V')))

        self.assertListEqual(
            list(parse_cols('H-l')),
            list(parse_cols('H-L')))

        self.assertListEqual(
            list(parse_cols('p-s')),
            list(parse_cols('P-S')))

    def test_bad_pattern(self):
        for s in ['A-3', '4-b', 'hello!', 'A_D', '', 'A to F', 'A - Z']:
            with self.assertRaises(ValueError):
                list(parse_cols(s))

    def test_multi_letter(self):
        result = parse_cols('CW-DD')
        self.assertListEqual(
            list(result),
            ['CW', 'CX', 'CY', 'CZ', 'DA', 'DB', 'DC', 'DD'])


class TestParseRows(unittest.TestCase):
    def test_parse_single(self):
        result = parse_rows('4-7')
        self.assertListEqual(
            list(result),
            [4, 5, 6, 7])

    def test_parse_multiple(self):
        result = parse_rows('2-5,9-12')
        self.assertListEqual(
            list(result),
            [2, 3, 4, 5, 9, 10, 11, 12])

    def test_misordered(self):
        self.assertListEqual(
            list(parse_rows('5-2')),
            list(parse_rows('2-5')))


    def test_bad_pattern(self):
        for s in ['A-3', '4-b', 'hello!', '4_7', '', '98 to 103', '12 - 56']:
            with self.assertRaises(ValueError):
                list(parse_rows(s))

    def test_multi_digit(self):
        result = parse_rows('95-103')
        self.assertListEqual(
            list(result),
            [95, 96, 97, 98, 99, 100, 101, 102, 103])


if __name__ == '__main__':
    unittest.main()
