from setuptools import setup, find_packages

with open('README.md', 'rt') as readme:
    long_description = readme.read()

setup(
    name='fsched',
    version='0.0.1',
    packages=find_packages(),

    author='Austin Bingham',
    author_email='austin@sixty-north.com',
    description='Festival scheduling',
    license='MIT License',
    keywords='scheduling',
    url = 'http://bitbucket.com/austin_bingham/fsched',
    classifiers=[
        'Development Status :: 4 - Beta',
        'Environment :: Console',
        'License :: OSI Approved :: MIT License',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3.3',
        'Programming Language :: Python :: 3.4',
    ],
    platforms='any',
    include_package_data=True,
    install_requires=[
        'docopt',
        'openpyxl'
    ],
    entry_points={
        'console_scripts': [
            'fsched = fsched.cli:main',
        ],
    },
    long_description=long_description,
)
